## 0.2.3 (2024-03-27)

### ✨ Features (3 changes)

- [TEST](vmillet-dev/he@30a81685fd8a4def239ffefdd00e27fa397844f9)
- [Change to come](vmillet-dev/he@a7b3adea7290c1802b57f5f0e14047e81ef072db)
- [Test](vmillet-dev/he@80183f4fc4fa9ff9ffc9d29de52be0664002b407)

## 0.2.2 (2024-03-27)

### ✨ Features (2 changes)

- [Change to come](vmillet-dev/he@a7b3adea7290c1802b57f5f0e14047e81ef072db)
- [Test](vmillet-dev/he@80183f4fc4fa9ff9ffc9d29de52be0664002b407)
