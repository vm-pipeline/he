# Clashbot-rs

## Features

## Usages

### Prerequis:
- ADB is installed on your system [See this section if not](INSTALL.md#install-adb)
- Developer options is enabled on your android device [How to?](https://developer.android.com/studio/debug/dev-options?hl=fr#enable)


You have two ways to connect your device :
### Over USB [Not Recommanded]

- Enable USB debbuging in developer options.
- Connect your device with a USB cable at your computer.
- Get the serial number of your android device with this command :

```
adb devices
```

- Use this number (first column) with the -s argument to start the bot on this specific device.

```
clashbot-rs[.exe] -s [SERIAL_NUMBER]
```

The app should be opened and handled by the bot. Have fun !

### Over WI-FI (Android 11+)

- Enable Debbuging over WI-FI.
- Case 1 : Device is not already paired (needed with WI-FI)
    - Ask pairing over wifi. Take note of the code and pairing port as well the ip and port device.

        ```
        clashbot-rs[.exe] -d [DEVICE_IP] -p [DEVICE_PORT]  -r [PAIRING_PORT] -c [CODE]
        ```
    - Next time, you can skip this step and just follow Case 2 by giving only your ip and port device as arguments.
- Case 2 : Device already paired
    - You just have to set up the IP and the port of the device by passing them as arguments :

    ```
    clashbot-rs[.exe] -d [DEVICE_IP] -p [DEVICE_PORT]
    ```

## Install

A complete guide can be found in [INSTALL.md](INSTALL.md)

## Credits

- [adb_client](https://docs.rs/adb_client/latest/adb_client/) (thx [@cocool97](https://github.com/cocool97)) - An Android Debug Bridge (ADB) client implementation in pure Rust ! 
- [imageproc](https://github.com/image-rs/imageproc) - use for template matching [Demo used here](https://github.com/image-rs/imageproc/blob/master/examples/template_matching.rs)
- [ocrs](https://github.com/robertknight/ocrs) - use for texts detection [Demo used here](https://github.com/robertknight/ocrs/blob/main/ocrs/examples/hello_ocr.rs)
- YOLOv8 implementation in rust - use for objects detection [Demo used here](https://github.com/ultralytics/ultralytics/tree/main/examples/YOLOv8-ONNXRuntime-Rust)

## Licence